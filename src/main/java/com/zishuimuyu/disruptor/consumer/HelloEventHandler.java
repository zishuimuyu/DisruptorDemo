package com.zishuimuyu.disruptor.consumer;

import com.zishuimuyu.disruptor.model.MessageVo;
import com.lmax.disruptor.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Description：消费者
 * @author：gjh
 * @createDate：2018/9/28
 * @company：CHAOS
 */
public class HelloEventHandler implements EventHandler<MessageVo> {
    Logger log = LoggerFactory.getLogger(HelloEventHandler.class);

    @Override
    public void onEvent(MessageVo event, long sequence, boolean endOfBatch) {
        try {
            //这里停止1000ms是为了确定消费消息是异步的
            Thread.sleep(1000);
            log.info("消费者处理消息开始");
            if (event != null) {
                log.info("消费者消费的信息是：{}", event.toString());
            }
        } catch (Exception e) {
            log.info("消费者处理消息失败");
        }
        log.info("消费者处理消息结束");
    }
}
