package com.zishuimuyu.disruptor.producer;


/**
 * @Description：Mqservice
 * @author：gjh
 * @createDate：2018/9/28
 * @company：CHAOS
 */
public interface DisruptorMqService {

    /**
     * 消息
     *
     * @param message
     */
    void sayHelloMq(String message);
}


