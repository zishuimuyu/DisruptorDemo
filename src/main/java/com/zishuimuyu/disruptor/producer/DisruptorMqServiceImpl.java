package com.zishuimuyu.disruptor.producer;

import com.zishuimuyu.disruptor.model.MessageVo;
import com.lmax.disruptor.RingBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @Description： Mqservice 实现类-生产者
 * @author：mc
 * @createDate：2018/9/28
 * @company：CHAOS
 */
@Component
@Service
public class DisruptorMqServiceImpl implements DisruptorMqService {
	Logger log = LoggerFactory.getLogger(DisruptorMqServiceImpl.class);

    @Autowired
    private RingBuffer<MessageVo> MessageVoRingBuffer;


    @Override
    public void sayHelloMq(String message) {
        log.info("消息记录: {}",message);
        //获取下一个Event槽的下标
        long sequence = MessageVoRingBuffer.next();
        try {
            //给Event填充数据
            MessageVo event = MessageVoRingBuffer.get(sequence);
            event.setValue(message);
            log.info("生产者往消息队列中添加消息,消息内容：{}", event.toString());
        } catch (Exception e) {
            log.error("生产者往消息队列中添加消息失败failed to add event to MessageVoRingBuffer for : e = {},{}",e,e.getMessage());
        } finally {
            //发布Event，激活观察者去消费，将sequence传递给改消费者
            //注意最后的publish方法必须放在finally中以确保必须得到调用；如果某个请求的sequence未被提交将会堵塞后续的发布操作或者其他的producer
            MessageVoRingBuffer.publish(sequence);
        }
    }
}

