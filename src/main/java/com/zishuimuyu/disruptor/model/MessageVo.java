package com.zishuimuyu.disruptor.model;

import lombok.Data;

/**
 * @Description：消息体Model
 * @author：gjh
 * @createDate：2018/9/28
 * @company：CHAOS
 */
@Data
public class MessageVo {

	private String code;
	
	private String value;


	
	
}
