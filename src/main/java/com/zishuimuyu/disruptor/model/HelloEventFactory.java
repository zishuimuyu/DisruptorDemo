package com.zishuimuyu.disruptor.model;

import com.lmax.disruptor.EventFactory;

/**
 * @Description：消息构造工厂
 * @author：gjh
 * @createDate：2018/9/28
 * @company：CHAOS
 */
public class HelloEventFactory implements EventFactory<MessageVo> {
    @Override
    public MessageVo newInstance() {
        return new MessageVo();
    }
}